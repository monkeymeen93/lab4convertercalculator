package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate;
	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		
		Intent i2 = this.getIntent();
		interestRate = i2.getFloatExtra("interestRate", 10.0f);
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
		EditText intRate = (EditText)findViewById(R.id.intRate);
		intRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText etRate = (EditText)findViewById(R.id.etRate);
		EditText intRate = (EditText)findViewById(R.id.intRate);
		try { //this activity is to sent back data
			float r = Float.parseFloat(etRate.getText().toString());
			float r2 = Float.parseFloat(intRate.getText().toString());
			//Create an integer to wrap the return value
			Intent data = new Intent();
			//Add the return value to intent
			data.putExtra("exchangeRate", r);
			data.putExtra("interestRate", r2);
			//Set the intent as the result when this activity ends
			this.setResult(RESULT_OK, data);
			//End this activity
			this.finish();
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			intRate.setText("");
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			intRate.setText("");
			etRate.setText("");
			etRate.requestFocus();
		}
	}

}
